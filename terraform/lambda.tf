# Créer le rôle pour la lambda avec aws_iam_role
resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}



# Créer une lambda fonction aws_lambda_function
resource "aws_lambda_function" "s3-func-lamda-Natha-Joseph" {
  filename      = "empty_lambda_code.zip"
  function_name = "aws_lambda_function"
  role          = "${aws_iam_role.iam_for_lambda.arn}"
  handler       = "lambda_main_app.lambda_handler"
  runtime       = "python3.7"
}

# Créer une aws_iam_policy pour le logging qui sera ajouté au rôle de la lambda
resource "aws_iam_policy" "lambda_logging" {
  name = "lambda_logging"
  path = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

# Attacher la policy de logging au rôle de la lambda avec aws_iam_role_policy_attachment
resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role = "${aws_iam_role.iam_for_lambda.name}"
  policy_arn = "${aws_iam_policy.lambda_logging.arn}"
}

# Attacher la policy de AmazonS3FullAccess au rôle de la lambda avec aws_iam_role_policy_attachment


# Autoriser la lambda à être déclenchée par un event s3 avec aws_lambda_permission

